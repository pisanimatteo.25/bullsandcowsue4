// Fill out your copyright notice in the Description page of Project Settings.
#include "BullCowCartridge.h"

void UBullCowCartridge::BeginPlay() // When the game starts
{
    Super::BeginPlay();
    const FString WordPath = FPaths::ProjectContentDir() / TEXT("WordList/HiddenWordList.txt");
    FFileHelper::LoadFileToStringArrayWithPredicate(Words, *WordPath, [](const FString& Word)
    {
        return Word.Len() >= 4 && Word.Len() <= 8 && IsIsogram(Word);
    });
    InitGame();
}

void UBullCowCartridge::OnInput(const FString& Input) // When the player hits enter
{
    if (bGameOver)
    {
        ClearScreen();
        InitGame();
    }
    else
    {
        ProcessGuess(Input);
    }
}

void UBullCowCartridge::InitGame()
{
    HiddenWord = Words[FMath::RandRange(0, Words.Num() - 1)];
    Lives = HiddenWord.Len();
    bGameOver = false;
    
    PrintLine(TEXT("Welcome to Bull & Cows"));
    PrintLine(TEXT("Guess the %i letter word"), HiddenWord.Len());
    
    PrintLine(TEXT("You have %i lives"), Lives);
    PrintLine(TEXT("Press enter to continue..."));

    // PrintLine(TEXT("The HW is %s "), *HiddenWord); Debug Line
}

void UBullCowCartridge::EndGame(FString Message)
{
    bGameOver = true;
    PrintLine(Message);
    PrintLine(TEXT("Press Enter to play again..."));
}


void UBullCowCartridge::ProcessGuess(const FString& Guess)
{
    if (Guess.Equals(HiddenWord))
    {
        EndGame("You Won!");
        return;
    }

    //Remove life
    PrintLine(TEXT("Lost a life"));
    --Lives;

    //Lives Ended
    if (Lives <= 0)
    {
        ClearScreen();
        PrintLine(TEXT("No lives left"));
        PrintLine(TEXT("Hidden word was: %s"), *HiddenWord);
        EndGame("You Loose");
        return;
    }
    //Check Isogram
    if(!IsIsogram(Guess))
    {
        PrintLine(TEXT("No repeating word, Try again"));
        return;
    }
    if (Guess.Len() != HiddenWord.Len())
    {
        PrintLine(FString::Printf(TEXT("Try Again \nHidden word length is %i"), HiddenWord.Len()));
        return;
    }
    //Check Bulls and Cows
    {
        BullsAndCowsCount(Guess, BCCount.BullsCount, BCCount.CowsCount);
        PrintLine(TEXT("Bulls: %i and Cows: %i"), BCCount.BullsCount, BCCount.CowsCount);
    }

}


bool UBullCowCartridge::IsIsogram(const FString& Word)
{
    //Foreach letter
    //Loop all chars in Word
    for(int i = 0; i<Word.Len();i++)
    {
        //Loop starting from the index we are
        //From length word less 1 (Cause could crash)
        for(int z = i; z<Word.Len()-1;z++)
        {
            //If equals return false
            if(Word[i] == Word[z+1])
                return false;
        }
    }
    //No char equals to other return true
    return true;
}

//Check words from list 
// TArray<FString> UBullCowCartridge::GetValidWords(const TArray<FString>& WordList) const
// {
//     TArray<FString> ValidWords;
//     for (FString Word: WordList)
//     {
//         if(Word.Len()>=4 && Word.Len() <= 8 && IsIsogram(Word))
//             ValidWords.Emplace(Word);
//     }
//     return ValidWords;
// }

void UBullCowCartridge::BullsAndCowsCount(const FString& Guess, int32& BullsCount, int32& CowsCount) const
{
    //Init out param
    BullsCount = 0;
    CowsCount = 0;
    //For entire word
    for(int i = 0; i<HiddenWord.Len();i++)
    {
        //If equal in index
        if(Guess[i] == HiddenWord[i])
        {
            //Bull ++ and next loop
            BullsCount++;
            continue;
        }
        //Else
        for (int z = 0; z<HiddenWord.Len();z++)
        {
            //If char present in the word
            //Cow ++
            if(Guess[i] == HiddenWord[z])
            {
                CowsCount++;
                break; //No more useless check
            }
        }
    }
}