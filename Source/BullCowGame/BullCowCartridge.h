// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Console/Cartridge.h"
#include "BullCowCartridge.generated.h"

struct FBullCowCount
{
	int32 BullsCount = 0;
	int32 CowsCount = 0;
};
UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class BULLCOWGAME_API UBullCowCartridge : public UCartridge
{
	GENERATED_BODY()

	public:
	virtual void BeginPlay() override;
	virtual void OnInput(const FString& Input) override;
	void InitGame();
	void EndGame(FString Message);
	
	//If typevar& without const will be out param
	void BullsAndCowsCount(const FString& Guess, int32& BullsCount, int32& CowsCount) const;
	void ProcessGuess(const FString& Guess);
	static bool IsIsogram(const FString& Word);
	// TArray<FString> GetValidWords(const TArray<FString>& WordList) const;
	
	// Your declarations go below!
	private:
	FString HiddenWord;
	int32 Lives;
	TArray<FString> Words;
	TArray<FString> IsogramWords;
	FBullCowCount BCCount;
	bool bGameOver;
};
